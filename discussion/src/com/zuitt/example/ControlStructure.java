package com.zuitt.example;

import java.util.Scanner;

public class ControlStructure {
    public static void main(String[] args) {
        //        [Section] Java Operators
        //        Arithmetic: -, +, /, *, %
        //        Comparison: <, >, >=, <=, ==, !=
        //        Logical: &&, ||, !
        //        Re/assignment operator: =, +=, -=, *=, /=
        //        Increment and decrement: i++, ++i, --i, i--

//        [Section] Selection Control Structure
//        statement allows us to manipulate the flow of the code depending on the evaluation of the condition;

        int num1 = 36;

        if (num1 % 5 == 0) {
            System.out.println(num1 + " is divisible by 5.");
        } else {
            System.out.println(num1 + " is not divisible by 5.");
        }

//        [Section] Short Circuiting
//        A technique applicable only to the AND and OR operators wherein if the statements or other control structure exits early by ensuring safety operation or for efficiency.
//        if(condition1 || condition2 || condition3 || condition4)

//        [Section] Ternary Operator
        int num2 = 24;
        boolean result = (num2 > 0) ? true : false;
        System.out.println(result);

//        [Section] Switch Statement:
//        are used to control the flow structures that allow one code block out of many other code blocks;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number");
        int input = scanner.nextInt();

        switch (input) {
            case 1 :
                System.out.println("North");
                break;
            case 2 :
                System.out.println("South");
                break;
            case 3 :
                System.out.println("East");
                break;
            case 4 :
                System.out.println("West");
                break;
            default :
                System.out.println("Invalid");
        }

    }
}
