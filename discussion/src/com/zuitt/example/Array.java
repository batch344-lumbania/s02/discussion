package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    //    [Section] Java Collection
    //    are a single unit of objects
    //    Useful for manipulating relevant pieces of data that can be used in different situtation, commonly collections are used in loops
    public static void main(String[] args) {
        //        This is where we add our codebase
        //        [Section] Array
        //        In Java, arrays are container of values of the same type given a predefined amount/number of values.
        //        Java arrays are more rigid, once the size and data type are defined, they can no longer be changed.

        //        Syntax: Array Declaration:
        //            datatype[] identifier = new dataType[numberOfElements];
        //           "[]" indicates that the data type should be able to hold multiple values.
        //           "new" keyword is used for non-primitive data types to tell Java to create the said variable.
        //           "numOfElements" will tell how many elements does our array can hold.

        //        Array declaration
        //        if we are going to declare an array of int[], the default value of the elements will all be 0.
        //        if array of String, the default value of the elements will be null
        int[] intArray = new int[5];

        //        to initialize the value of out elements inside the array, we are going to use the index.
        intArray[0] = 200;
        intArray[1] = 122;
        intArray[2] = 322;

        //        This will just print out the memory address of the Array.
        System.out.println(intArray);

        //        To print out the intArray, we need to import the Array Class and use the .toString() method.
        //        This method will convert the array as a string in the terminal.
        System.out.println(Arrays.toString(intArray));

        //        Declaration and Initialization of an Array
        //        Syntax:
        //            dataType[] identifier = {elementA, elementB, ...};
        //        Compiler automatically specifies the size by counting the number of elements during the initialization.

        String[] names = {"Seth", "Timothy", "Ayka"};

        System.out.println(Arrays.toString(names));
        System.out.println(names.length);

        //        Sample java array methods
        //        sort method

        Arrays.sort(intArray);
        System.out.println("Order of items after sort: " + Arrays.toString(intArray));

        //        Multidimensional array
        //        A two-dimensional array can be described by two lengths nested within each other, like a matrix.
        //        First length is row, second length is the column. arrayName[][]

        String[][] classroom = new String[3][3];
        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        //Third Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofey";

        //        This is only applicable to those two-dimensional arrays
        System.out.println(Arrays.deepToString(classroom));

        //        Note: In Java, the size of the array cannot be modified. If there is a need to add or remove elements, you need to create a new array.

        //        [Section] ArrayList
        //        Resizable arrays, wherein elements can be added or removed whenever it is needed.
        //        Syntax:
        //          ArrayList<T> identifier = new ArrayList<T>();
        //        <T> is used to specify that the list can only have one type of object in a collection.
        //        ArrayList cannot hold primitive data types, "java wrapper classes" provide a way to use this types as object.
        //        In short, Object version of the primitive data type with methods.

        //        Declaration of an ArrayList
        ArrayList<Integer> numbers = new ArrayList<Integer>();

        //        Add elements
        numbers.add(1);
        System.out.println(numbers);

        //        Access specific element
        System.out.println(numbers.get(0));

        //        Declaration with Initialization:
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane", "Mike"));

        //        DOES NOT WORK. ARRAYLIST ARGUMENT ONLY ACCEPTS ONE PARAMETER ArrayList<String> yonko = new ArrayList<String>("Kaido","Luffy");

        System.out.println(students);
        students.add("John");
        System.out.println(students);

        //        Updating an element
        //        arrayListName.set(index, updatedValue);

        students.set(2, "Juan");
        System.out.println(students);

        //        remove a specific element
        //        arrayListName.remove(index);
        students.remove(1);
        System.out.println(students);

        //        Getting the arrayList size
        //        arrayListName.size();

        System.out.println(students.size());

        //        Removing all elements
        //        arrayListName.clear();

        students.clear();
        System.out.println(students);
        System.out.println(students.size());

        //        [Section] Hashmaps
        //        most objects in Java are defined and are instantiated of Classes that contains a proper set of properties and methods.
        //        There might be use cases where it is not appropriate, or you may simply want a collection of data in key-value pairs.
        //        In Java, "keys" are also referred as "fields" where in the values can be accessed through fields.
        //        Syntax:
        //            HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>();

        //        Declaration
        HashMap<String, String> jobPosition = new HashMap<String, String>();

        System.out.println(jobPosition);

        //        Methods in Hashmap:
        //        Add key-value pair
        //        HashMapName.put(fieldName, fieldValue);
        jobPosition.put("Student", "Brandon");
        jobPosition.put("Dreamer", "Alice");
        //        When assigning another value in a already existing field, the value will be overridden.
        jobPosition.put("Student", "John");
        System.out.println(jobPosition);

        //        Accessing element = we use field name because they are unique.
        //        hashMapName.get("fieldName");

        //        If fieldName does not exist on the hashmap, it will return a value of null
        System.out.println(jobPosition.get("Student"));

        //        Updating values
        //        hashMapName.replace("fieldNameToChange", "newValue");
        jobPosition.replace("Student", "John Doe");
        System.out.println(jobPosition);

        //        Remove an element
        //        hashMapName.remove("key");
        //        jobPosition.remove("Dreamer");
        //        System.out.println(jobPosition);

        //        Retrieve hashmap keys
        //        hashMapName.keySet();
        System.out.println(jobPosition.keySet());

        //        Retrieve the values from hashmap
        //        hashMapName.values();
        System.out.println(jobPosition.values());

        //        Remove all key-value pairs
        //        hashMapName.clear();
        jobPosition.clear();
        System.out.println(jobPosition);

//        Declaration of HashMap with Initialization
        HashMap<String, String> jobPosition2 = new HashMap<String, String>(){
            {
                put("Teacher", "John");
                put("Artists", "Van Gogh");
                put("Fighter", "Chou");
            }
        };

        System.out.println(jobPosition2);
    }
}
